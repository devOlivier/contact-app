import React from 'react'
import Header from './components/Header'
import Liste from './components/Liste'
import AddContact from './components/AddContact'
import Apropos from './components/Apropos'
import ErrorNotFound from './components/ErrorNotFound'
import { Provider } from './context'
import { BrowserRouter as Router , Route , Switch } from 'react-router-dom'


class App extends React.Component {
  render() {
    return (
      <Provider>
        <Router>
          <div className="app">
            <Header />
            <div className="container">
              <h1>Gestionnaire de contacts</h1>
              <Switch>
                <Route exact path="/add" component={AddContact} />
                <Route exact path="/" component={Liste} />
                <Route exact path="/apropos" component={Apropos} />
                <Route component={ErrorNotFound} />
              </Switch>
            </div>
          </div>
        </Router>
      </Provider>
    );
  }
}
export default App;



