import React from 'react'
const Context = React.createContext()
//reducer => chef d'orchestre des action sur la state
//Il faudra ensuite ce créer un dispatch dans notre state pour nos fonctions
//on garde uniquement sur ce qu'on a pas cliqué => payload => action event sur quoi on a cliqué
const reducer = (state, action) => {
    switch (action.type) {
        case 'DELETE_CONTACT':
            return {
                profil: state.profil.filter(contact => contact.id !== action.payload)
            }
        default:
            return state
    }
}

export class Provider extends React.Component {
    state = {
        profil: [
            {
                id: 1,
                nom: 'Olivier Clercq',
                ville: 'Lille',
                tel: '06 03 00 00 00',
                tarif: '300€',
                picture: 'https://randomuser.me/api/portraits/lego/3.jpg',
                tech: 'Javascript , React , Redux'
            },
            {
                id: 2,
                nom: 'Sandra Tillemans',
                ville: 'Lille',
                tel: '06 54 00 00 00',
                tarif: '330€',
                picture: 'https://randomuser.me/api/portraits/women/18.jpg',
                tech: 'Javascript , Angular , MongoDB'
            },
            {
                id: 3,
                nom: 'Mathieu Dewitt',
                ville: 'Croix',
                tel: '06 78 00 00 00',
                tarif: '500€',
                picture: 'https://randomuser.me/api/portraits/men/21.jpg',
                tech: 'Symfony , Java , Sql'
            }

        ],
        dispatch: action => {
            this.setState(state => reducer(state, action))
        }
    }
    render() {
        return (
            <Context.Provider value={this.state}>
                {this.props.children}
            </Context.Provider>
        )
    }
}
export const Consumer = Context.Consumer


