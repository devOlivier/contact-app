import React from 'react'
import { Consumer } from '../context'
import uuid from 'uuid'

class AddContact extends React.Component {
    //Juste pour l'exemple
    state = {
        nom : '',
        ville : '',
        tel : ''
    }

    onChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    onSubmit = (dispatch, e) => {
        e.preventDefault()
        const newContact = {
            id: uuid(),
            nom : this.state.nom,
            ville: this.state.ville,
            tel: this.state.tel
        }
        dispatch({ type: 'ADD_CONTACT' , payload : newContact})

        this.setState({
            nom: '',
            ville: '',
            tel: ''
        })
        alert('Création du contact réussi')

        this.props.history.push('/liste')
    }

    render () {

        return (
            <Consumer>
                {value => {
                    return (
                        <div className = "row" >
                            <form className="col s6" onSubmit={this.onSubmit.bind(this , value.dispatch)}>
                                <div className="input-field col s12">
                                    <input
                                        type="text"
                                        name="nom"
                                        placeholder="Entrez un nom et prénom"
                                        value={this.state.nom}
                                        onChange={this.onChange}
                                    />
                                </div>
                                <div className="input-field col s12">
                                    <input
                                        type="text"
                                        name="ville"
                                        placeholder="Entrez une ville"
                                        value={this.state.ville}
                                        onChange={this.onChange}
                                    />
                                </div>
                                <div className="input-field col s12">
                                    <input
                                        type="text"
                                        name="tel"
                                        placeholder="Entrez un téléphone"
                                        value={this.state.tel}
                                        onChange={this.onChange}
                                    />
                                </div>

                                <React.Fragment>
                                    <button type="submit">click</button>
                                </React.Fragment>
                            </form>
                        </div> 
                         )
                     }}
            </Consumer>
        )
    }
}

export default AddContact