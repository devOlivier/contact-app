import React from 'react'

function Apropos () {
    return (
        <React.Fragment>
            <h3>Version 1.0</h3>
            <div className="contentVersion">
                <h2>Note de mise à jour</h2>
                <ul>
                    <li>Update ...</li>
                    <li>Update ...</li>
                    <li>Update ...</li>
                    <li>Update ...</li>
                    <li>Update ...</li>
                </ul>
            </div>
        </React.Fragment>
    )
}
export default Apropos