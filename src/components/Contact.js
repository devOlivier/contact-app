import React from 'react'
import { Consumer } from '../context'

class Contact extends React.Component {
    state = {
        show: false
    }

    retractCard = () => {
        this.setState({
            show: !this.state.show
        })
        console.log(this.state.show)
    }

    deleteContact = (id, dispatch) =>
        dispatch({ type: 'DELETE_CONTACT', payload: id })


    render() {
        return (
            <Consumer>
                {value => {
                    return (
                        <div className="col s12 m6 l4">
                            <div className="card blue-grey darken-3">
                                <div className="card-content white-text">
                                    <span className="card-title"> {this.props.nom} &nbsp;
                            {this.state.show ? (
                                            <i className="material-icons"
                                                onClick={this.retractCard}
                                                style={{ cursor: 'pointer' }}>arrow_drop_up
                                </i>
                                        ) : <i className="material-icons"
                                            onClick={this.retractCard}
                                            style={{ cursor: 'pointer' }}>arrow_drop_down
                                </i>}

                                    </span>
                                    {this.state.show ? (
                                        <ul>
                                            <li> {this.props.ville} - {this.props.tel} </li>
                                            <li> {this.props.tarif} </li>
                                            <li> <img
                                                style={{ width: 70, height: 70, borderRadius: 50, marginTop: 15 }}
                                                src={this.props.picture} /> </li>
                                            <li style={{ marginTop: 30 }} > {this.props.tech}</li>
                                        </ul>
                                    ) : null}
                                </div>
                                <div className="card-action">
                                    <a className="btn-floating btn-small cyan pulse">
                                        <i className="material-icons">border_color</i></a>
                                    <a className="btn-floating btn-small waves-effect waves-light red"
                                        onClick={() => this.deleteContact(this.props.id, value.dispatch)}
                                        style={{ float: "right" }}>
                                        <i className="material-icons">close</i></a>
                                </div>
                            </div>
                        </div>
                    )
                }}
            </Consumer>
        )
    }
}
export default Contact
