import React from 'react'
import { Link } from 'react-router-dom'

function Header() {
    return (
        <div className="navApp">
            <nav className="#039be5 light-blue darken-1">
                <div className="nav-wrapper">
                   
                    <ul id="nav-mobile" class="right hide-on-med-and-down">   
                        <li><Link to="/">Accueil</Link></li>
                        <li><Link to="/add">Ajouter</Link></li>
                        <li><Link to="/apropos">Version</Link></li>
                    </ul>
                </div>
                
            </nav>
        </div>
    )
}
export default Header
