import React from 'react'
import Contact from './Contact'
import { Consumer } from '../context'

class Liste extends React.Component {
    render() {
        return (
            <Consumer>
                {value => {
                    return (
                        <React.Fragment>
                            {value.profil.map(user => (
                                <Contact
                                    key={user.id}
                                    id={user.id}
                                    nom={user.nom}
                                    ville={user.ville}
                                    tel={user.tel}
                                    tarif={user.tarif}
                                    picture={user.picture}
                                    tech={user.tech}
                                />
                            ))}
                        </React.Fragment>
                    )
                }}
            </Consumer>
        )
    }
}
export default Liste

